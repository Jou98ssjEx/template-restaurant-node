const express = require('express');

class Server {

    constructor() {

        this.app = express();
        this.port = process.env.PORT;

        // Paths
        // this.userPath = '/api/user';
        // this.authPath = '/api/auth';
        // this.paths = {
        //     auth: '/api/auth',
        //     category: '/api/category',
        //     product: '/api/product',
        //     search: '/api/search',
        //     user: '/api/user',
        //     upload: '/api/upload',
        // }

        // // dbConect
        // this.dbConect();

        this.midellawers();

        // this.routes();

    }

    // Methods

    // async dbConect() {
    //     await ConectDB();
    // }

    midellawers() {

        // // Cors
        // this.app.use(cors());

        // Lectura y parseo JSON
        this.app.use(express.json());

        this.app.use(express.static('src/public'));

        // File upload
        // aceptar carga de archivo en la APIRest
        // this.app.use(fileUpload({
        //     useTempFiles: true,
        //     tempFileDir: '/temp',
        //     createParentPath: true
        // }))
    }

    // routes() {

    //     // this.app.get('/', (req, res) => {
    //     //     res.send('Hola');
    //     // });

    //     // this.app.route('/api/user', require('../routes/user.routes'));
    //     this.app.use(this.paths.auth, require('../routes/auth.routes'));
    //     this.app.use(this.paths.category, require('../routes/category.routes'));
    //     this.app.use(this.paths.product, require('../routes/product.routes'));
    //     this.app.use(this.paths.search, require('../routes/search.routes'));
    //     this.app.use(this.paths.user, require('../routes/user.routes'));
    //     this.app.use(this.paths.upload, require('../routes/upload.routes'));
    // }

    listener() {

        this.app.listen(this.port, () => {
            console.log(`Server online in port ${this.port}`);
        })
    }


}

module.exports = Server;